import UIKit

class ViperEmbedSegue: UIStoryboardSegue {
    
    override func perform() {
        guard let identifier = identifier, let embeddableViewController = source as? ViperEmbedSegueViewContainerProtocol else {
            return
        }
        
        let containerView = embeddableViewController.viewFor(embedIdentifier: identifier)
        source.embed(childViewController: destination, toContainerView: containerView)
    }
    
}

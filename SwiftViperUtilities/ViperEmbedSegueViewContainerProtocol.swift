import UIKit

protocol ViperEmbedSegueViewContainerProtocol {
    
    func viewFor(embedIdentifier: String) -> UIView
    
}
